extends Node

var board : Node2D
var board_rect = Rect2(0, 100, 256, 256)
var tile_size : int  = 32
var pieces_on_the_board = []

var player_color : int

var selected_piece : BoardPiece = null
var grabbed_piece_tile_pos : Vector2

onready var green_sprite = load("res://Board/Green Sprite.tscn")
var move_sprites = []
var selected_piece_valid_tiles = []

var self_timer : Timer
var opponent_timer : Timer

var network

func construct(_player_color, _player_time, _opponent_time, _pieces):
	board = get_parent()
	network = board.network
	
	player_color = _player_color
	pieces_on_the_board = _pieces
	
	
	for piece in _pieces:
		add_child(piece)
		board.set_tile(piece.tile_pos, piece.get_class(), piece.color)
		piece.set_global_pos(tile_pos_to_global(piece.tile_pos))
		if(piece.color == player_color):
			piece.connect("grabbed", self, "piece_got_grabbed")
			piece.connect("dropped", self, "piece_got_dropped")
			piece.connect("clicked", self, "piece_got_clicked")
	
	
	#setup the timers
	self_timer = Timer.new()
	self_timer.wait_time = 60 * 10
	self_timer.one_shot = true
	opponent_timer = Timer.new()
	opponent_timer.wait_time = 60 * 10
	opponent_timer.one_shot = true
	add_child(self_timer)
	add_child(opponent_timer)
	self_timer.start()
	opponent_timer.start()
	if(player_color == Global.colors.RED):
		enable_pieces()
		opponent_timer.set_paused(true)
	else:
		self_timer.set_paused(true)
	
	#this doesn't actually start the timers since they're paused
	#it is used to set the time_left variable
	self_timer.start(_player_time)
	opponent_timer.start(_opponent_time)


func _process(_delta):
	get_node("../Player Time").text = Global.format_time(self_timer.time_left)
	get_node("../Opponent Time").text = Global.format_time(opponent_timer.time_left)


func _input(event):
	if(selected_piece == null): return
	if(event is InputEventMouseButton and event.button_index == 1):#left click
		if(board_rect.has_point(event.position)):
			var x = int((event.position.x-board_rect.position.x)/ tile_size)
			var y = int((event.position.y-board_rect.position.y) / tile_size)
			if((Vector2(x,y) in selected_piece_valid_tiles) == false): 
				selected_piece.set_global_pos(tile_pos_to_global(selected_piece.tile_pos))
			else:
				perform_move(selected_piece, Vector2(x, y))


func enable_pieces():
	for piece in pieces_on_the_board:
		if(piece.color == player_color):
			piece.enabled  = true

func disable_pieces():
	for piece in pieces_on_the_board:
		piece.enabled = false


func piece_got_grabbed(piece):
	board.set_tile(piece.tile_pos, Global.pieces.EMPTY, player_color)
	grabbed_piece_tile_pos = piece.tile_pos
	select_piece(piece)

func piece_got_dropped(piece):
	var x = int(piece.global_position.x / tile_size)
	var y = int((piece.global_position.y-board_rect.position.y) / tile_size)
	if((Vector2(x,y) in selected_piece_valid_tiles) == false):
		piece.set_global_pos(tile_pos_to_global(piece.tile_pos))
		deselect_piece()
	else:
		perform_move(piece, Vector2(x, y))

func piece_got_clicked(piece):
	if(selected_piece == piece):
		deselect_piece()
	else:
		select_piece(piece)


func show_possible_moves():
	for move in selected_piece_valid_tiles:
		var block = green_sprite.instance()
		add_child(block)
		move_sprites.append(block)
		block.global_position = Vector2(board_rect.position.x + move.x*tile_size + tile_size/2, 
		  board_rect.position.y + move.y*tile_size + tile_size/2)

func hide_possible_moves():
	for sprite in move_sprites:
		remove_child(sprite)
	move_sprites.clear()


func move_piece_to_tile(piece, x, y):
	board.set_tile(piece.tile_pos, Global.pieces.EMPTY, player_color)
	if(board.get_tile_type(x, y) != Global.pieces.EMPTY):
		var taken_piece = get_piece_by_tile(x, y)
		pieces_on_the_board.erase(taken_piece)
		taken_piece.queue_free()
	board.set_tile(Vector2(x, y), piece.get_class(), piece.color)
	piece.tile_pos = Vector2(x, y)
	piece.set_global_pos(Vector2(tile_size*x+tile_size/2, 
		tile_size*y+tile_size/2) + board_rect.position)


func tile_pos_to_global(vec):
	return Vector2(vec.x * tile_size, vec.y * tile_size) + board_rect.position + Vector2(16,16)

func select_piece(piece):
	if(selected_piece == piece): return
	selected_piece = piece
	selected_piece_valid_tiles = piece._get_move_tiles(board)
	hide_possible_moves()
	show_possible_moves()

func deselect_piece():
	selected_piece = null
	selected_piece_valid_tiles.clear()
	hide_possible_moves()


func perform_move(piece, tile_pos):
	deselect_piece()
	if(piece.tile_pos == tile_pos): 
		board.set_tile(piece.tile_pos, piece.get_class(), piece.color)
		return
	
	var old_tile_pos = piece.tile_pos
	move_piece_to_tile(piece, tile_pos.x, tile_pos.y)
	network.rpc_id(1, "move_performed", old_tile_pos, piece.tile_pos)
	disable_pieces()
	opponent_timer.set_paused(false)
	self_timer.set_paused(true)

func opponent_moved(old_tile_pos, tile_pos):
	move_piece_to_tile(get_piece_by_tile(old_tile_pos), tile_pos.x, tile_pos.y)
	enable_pieces()
	opponent_timer.set_paused(true)
	self_timer.set_paused(false)

func set_time(first, second):
	if(player_color == Global.colors.RED):
		self_timer.wait_time = first
		opponent_timer.wait_time = second
	elif(player_color == Global.colors.YELLOW):
		self_timer.wait_time = second
		opponent_timer.wait_time = first

func game_ends(winner):
	var popup = load("res://Board/Battle Phase/Game End Popup.tscn").instance()
	add_child(popup)
	popup.set_winner_name(winner)
	popup.popup_centered()


func get_piece_by_tile(pos, y = null):
	if(y != null):
		pos = Vector2(pos, y)
	
	for piece in pieces_on_the_board:
		if(piece.tile_pos == pos):
			return piece




