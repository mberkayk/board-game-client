extends Node2D

var phase_node:Node

var board_tiles = []

var network


func construct(_player_name, _opponent_name, _player_color,
			_player_time, _opponent_time, _current_phase, _pieces):
	
	for i in range(8):#iterate through columns,the x coordinate
		board_tiles.append([])
		for _j in range(8): #iterate through rows, the y coordinate
			board_tiles[i].append(Global.pieces.EMPTY | Global.colors.RED)
	
	get_node("Player Name").text = _player_name
	get_node("Opponent Name").text = _opponent_name
	
	network = get_parent()
	
	if(_current_phase == Global.phases.PREP):
		phase_node = load("res://Board/Preparation Phase/Preparation Phase.tscn").instance()
	elif(_current_phase == Global.phases.BATTLE):
		phase_node = load("res://Board/Battle Phase/Battle Phase.tscn").instance()
	
	add_child(phase_node)
	phase_node.construct(_player_color, _player_time, _opponent_time, _pieces)


func switch_to_battle_phase():
	
	var player_color = phase_node.player_color
	
	var pieces = phase_node.pieces
	phase_node.disconnect_piece_signals()
	for piece in pieces:
		piece.get_parent().remove_child(piece)
	
	remove_child(phase_node)
	
	phase_node = load("res://Board/Battle Phase/Battle Phase.tscn").instance()
	add_child(phase_node)
	phase_node.construct(player_color, 600, 600, pieces)


func set_tile(_pos, _piece_type, _color):
	board_tiles[_pos.x][_pos.y] = _piece_type | _color


func get_tile_color(x, y):
	return board_tiles[x][y] & 0b11000

func get_tile_type(x, y):
	return board_tiles[x][y] & 0b111


### Prep Phase Remote Functions ###
func opponent_placed_a_piece(piece_type, coordinate):
	phase_node.opponent_placed_a_piece(piece_type, coordinate)

func players_turn():
	phase_node.players_turn()

### Battle Phase Functions ###
func opponent_moved(from_adjusted, to_adjusted):
	phase_node.opponent_moved(from_adjusted, to_adjusted)

func set_time(first_timer, second_timer):
	phase_node.set_time(first_timer, second_timer)

func game_ends(winner):
	phase_node.game_ends(winner)


