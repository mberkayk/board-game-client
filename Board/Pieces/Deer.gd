extends BoardPiece



func get_class(): return Global.pieces.DEER

func construct(_col, _tile_pos, _global_pos, _enabled):
	.construct(_col, _tile_pos, _global_pos, _enabled)
	_set_color(_col)

func _set_color(_color):
	._set_color(_color)
	if(_color == Global.colors.YELLOW):
		get_node("Sprite").texture = load("res://Board/Pieces/Deer Yellow.png")

func _get_move_tiles(board):
	var result = []
	
	#horizontal and vertical moves
	for i in range(1, 8-tile_pos.x):
		if(board.get_tile_type(tile_pos.x+i, tile_pos.y) != Global.pieces.EMPTY):
			if(board.get_tile_color(tile_pos.x+i, tile_pos.y) == color):
				break
			else:
				result.append(Vector2(tile_pos.x+i, tile_pos.y))
				break
		result.append(Vector2(tile_pos.x+i, tile_pos.y))
	
	for i in range(1, tile_pos.x+1):
		if(board.get_tile_type(tile_pos.x-i, tile_pos.y) != Global.pieces.EMPTY):
			if(board.get_tile_color(tile_pos.x-i, tile_pos.y) == color):
				break
			else:
				result.append(Vector2(tile_pos.x-i, tile_pos.y))
				break
		result.append(Vector2(tile_pos.x-i, tile_pos.y))
	
	for i in range(1, 8-tile_pos.y):
		if(board.get_tile_type(tile_pos.x, tile_pos.y+i) != Global.pieces.EMPTY):
			if(board.get_tile_color(tile_pos.x, tile_pos.y+i) == color):
				break
			else:
				result.append(Vector2(tile_pos.x, tile_pos.y+i))
				break
		result.append(Vector2(tile_pos.x, tile_pos.y+i))
	
	for i in range(1, tile_pos.y+1):
		if(board.get_tile_type(tile_pos.x, tile_pos.y-i) != Global.pieces.EMPTY):
			if(board.get_tile_color(tile_pos.x, tile_pos.y-i) == color):
				break
			else:
				result.append(Vector2(tile_pos.x, tile_pos.y-i))
				break
		result.append(Vector2(tile_pos.x, tile_pos.y-i))
	
	return result
