class_name BoardPiece
extends Node2D


signal grabbed(node)
signal dropped(node)
signal clicked(node)

var color : int

var mouse_is_pressed = false
var sprite : Sprite
var sprite_rect : Rect2

var enabled : bool

var tile_pos : Vector2

var grab_pos : Vector2

var sent_grabbed_signal = false

func construct(_col, _tile_pos, _global_pos, _enabled):
	color = _col
	
	tile_pos = _tile_pos
	set_global_pos(_global_pos)
	enabled  = _enabled
	
	sprite = get_node("Sprite")
	var size = sprite.texture.get_size()
	sprite_rect = Rect2(global_position - size/2, size)


func _input(event):
	if(enabled == false): return
	if(event is InputEventMouseButton):
		if(sprite_rect.has_point(event.position)):
			if(event.pressed and event.button_index == BUTTON_LEFT):
				mouse_is_pressed = true
				grab_pos = event.position
				sent_grabbed_signal = false
			else:
				if(mouse_is_pressed and event.button_index == BUTTON_LEFT):
					mouse_is_pressed = false
					if(event.position.distance_squared_to(grab_pos) > 2*2):
						emit_signal("dropped", self)
					else:
						emit_signal("clicked", self)
	
	if(event is InputEventMouseMotion):
		if(mouse_is_pressed):
			if(event.position.distance_squared_to(grab_pos) > 2*2 and
			sent_grabbed_signal == false):
				emit_signal("grabbed", self)
				sent_grabbed_signal = true
			if(sent_grabbed_signal):
				set_global_pos(event.position)


func _set_color(_color):#this is a virtual function
	pass

func _get_move_tiles(_board_info):#virtual function
	pass

func set_global_pos(vec):
	global_position = vec
	sprite_rect.position = vec - Vector2(16, 16)
