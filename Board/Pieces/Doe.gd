extends BoardPiece



func get_class(): return Global.pieces.DOE

func construct(_col, _tile_pos, _global_pos, _enabled):
	.construct(_col, _tile_pos, _global_pos, _enabled)
	_set_color(_col)

func _set_color(_color):
	._set_color(_color)
	if(color == Global.colors.YELLOW):
		sprite.texture = load("res://Board/Pieces/Doe Yellow.png")


func _get_move_tiles(board):
	var result = []
	
	for i in range(-1,2):
		for j in range(-1, 2):
			if(i == 0 and j == 0): continue
			var x:int = i + tile_pos.x
			var y:int = j + tile_pos.y
			if(x < 0 or x > 7 or y > 7 or y < 0): continue
			if(board.get_tile_type(x, y) != Global.pieces.EMPTY and
				board.get_tile_color(x, y) == color):
					continue
			result.append(Vector2(x, y))
	
	return result
