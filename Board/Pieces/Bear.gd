extends BoardPiece


func get_class(): return Global.pieces.BEAR

func construct(_col, _tile_pos, _global_pos, _enabled):
	.construct(_col, _tile_pos, _global_pos, _enabled)
	_set_color(_col)

func _set_color(_color):
	._set_color(_color)
	if(color == Global.colors.YELLOW):
		get_node("Sprite").texture = load("res://Board/Pieces/Bear Yellow.png")

func _get_move_tiles(board):
	var result = []
	#diagonal moves
	for x_dir in [-1,1]:
		for y_dir in [-1,1]:
			var x = tile_pos.x
			var y = tile_pos.y
			while ((x+x_dir in range(0,8)) and (y+y_dir in range(0,8))):
				x += x_dir
				y += y_dir
				if(board.get_tile_type(x, y) != Global.pieces.EMPTY):
					if(board.get_tile_color(x, y) == color):
						break
					else:
						result.append(Vector2(x, y))
						break
				result.append(Vector2(x, y))
	
	#horizontal and vertical moves
	for i in range(1,4):
		if(tile_pos.x+i > 7): break
		if(board.get_tile_type(tile_pos.x+i, tile_pos.y) != Global.pieces.EMPTY):
			if(board.get_tile_color(tile_pos.x+i, tile_pos.y) == color):
				break
			else:
				result.append(Vector2(tile_pos.x+i, tile_pos.y))
				break
		result.append(Vector2(tile_pos.x+i, tile_pos.y))
	
	for i in range(1,4):
		if(tile_pos.x-i < 0): break
		if(board.get_tile_type(tile_pos.x-i, tile_pos.y) != Global.pieces.EMPTY):
			if(board.get_tile_color(tile_pos.x-i, tile_pos.y) == color):
				break
			else:
				result.append(Vector2(tile_pos.x-i, tile_pos.y))
				break
		result.append(Vector2(tile_pos.x-i, tile_pos.y))
	
	for i in range(1,4):
		if(tile_pos.y+i > 7): break
		if(board.get_tile_type(tile_pos.x, tile_pos.y+i) != Global.pieces.EMPTY):
			if(board.get_tile_color(tile_pos.x, tile_pos.y+i) == color):
				break
			else:
				result.append(Vector2(tile_pos.x, tile_pos.y+i))
				break
		result.append(Vector2(tile_pos.x, tile_pos.y+i))
	
	for i in range(1,4):
		if(tile_pos.y-i < 0): break
		if(board.get_tile_type(tile_pos.x, tile_pos.y-i) != Global.pieces.EMPTY):
			if(board.get_tile_color(tile_pos.x, tile_pos.y-i) == color):
				break
			else:
				result.append(Vector2(tile_pos.x, tile_pos.y-i))
				break
		result.append(Vector2(tile_pos.x, tile_pos.y-i))
	
	return result

