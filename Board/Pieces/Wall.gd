extends BoardPiece


func get_class(): return Global.pieces.WALL

func construct(_col, _tile_pos, _global_pos, _enabled):
	.construct(_col, _tile_pos, _global_pos, _enabled)
	_set_color(_col)

func _set_color(_color):
	._set_color(_color)
	if(color == Global.colors.YELLOW):
		get_node("Sprite").texture = load("res://Board/Pieces/Wall Yellow.png")
