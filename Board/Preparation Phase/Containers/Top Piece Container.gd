class_name TopPieceContainer
extends Node2D

var piece_count : int
var piece_type : int
var piece_scene : Resource
var piece : BoardPiece

var color : int


func construct(_piece_type, _col):
	_set_piece_type(_piece_type)
	color = _col
	add_a_piece()
	piece.enabled = false
	_update_textbox()

func _set_piece_type(_piece_type):
	piece_type = _piece_type
	
	if(piece_type == Global.pieces.DOE):
		piece_scene = load("res://Board/Pieces/Doe.tscn")
		piece_count = 1
	elif(piece_type == Global.pieces.DEER):
		piece_scene = load("res://Board/Pieces/Deer.tscn")
		piece_count = 2
	elif(piece_type == Global.pieces.BEAR):
		piece_scene = load("res://Board/Pieces/Bear.tscn")
		piece_count = 2
	elif(piece_type == Global.pieces.WALL):
		piece_scene = load("res://Board/Pieces/Wall.tscn")
		piece_count = 4


func add_a_piece():
	piece = piece_scene.instance()
	add_child(piece)
	piece.construct(color, Vector2(-1,-1), self.global_position+Vector2(16,16), true)

func decrement_piece_count():
	piece_count -= 1
	if(piece_count == 0):
		piece.queue_free()
	_update_textbox()


func _update_textbox():
	get_node("Label").text = "X" + str(piece_count)
