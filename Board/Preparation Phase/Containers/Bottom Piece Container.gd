extends TopPieceContainer

func construct(_piece_type, _col):
	.construct(_piece_type, _col)
	piece.enabled = true


func add_a_piece():
	.add_a_piece()
	piece.connect("grabbed", get_parent(), "_on_piece_grabbed")
	piece.connect("dropped", get_parent(), "_on_piece_dropped")
	piece.connect("clicked", get_parent(), "_on_piece_clicked")


func piece_got_grabbed():
	piece_count -= 1
	if(piece_count > 0):
		add_a_piece()
	else:
		piece = null
	_update_textbox()

func piece_got_dropped(node):
	if(piece_count == 0):
		piece = node
		piece_count = 1
	else:
		node.queue_free()
		piece_count += 1
	piece.enabled = true
	_update_textbox()


func enable_pieces(par):
	if(piece == null): return
	piece.enabled = par
