extends Node

var board : Node2D
var board_rect = Rect2(0, 100, 256, 256)
var tile_size:int = 32

var self_timer:Timer
var opponent_timer:Timer

var player_color:int = Global.colors.RED
var opponent_color:int = Global.colors.YELLOW
var pieces = []

enum states{OPPONENT, PLAYER}
var current_state : int

onready var green_sprite_scene = load("res://Board/Green Sprite.tscn")
onready var red_sprite_scene = load("res://Board/Red Sprite.tscn")
var current_sprites = []

var anim:AnimationPlayer #start placing your pieces animation

var network

func construct(_player_color, _player_time, _opponent_time, _pieces):
	
	board = get_parent()
	network = board.network
	
	_set_color(_player_color)
	
	_construct_containers()
	
	if(player_color == Global.colors.YELLOW):
		_enable_containers(false)
	
	#setup the timers
	self_timer = Timer.new()
	opponent_timer = Timer.new()
	self_timer.one_shot = true
	opponent_timer.one_shot = true
	self_timer.wait_time = 60
	opponent_timer.wait_time = 60
	add_child(self_timer)
	add_child(opponent_timer)
	
	#this doesn't actually start the timers
	#it is used to set the time_left variable
	self_timer.start(_player_time)
	opponent_timer.start(_opponent_time)
	
	if(player_color == Global.colors.RED):
		opponent_timer.stop()
		current_state = states.PLAYER
	else:
		self_timer.stop()
		current_state = states.OPPONENT
	
	
	get_node("../Player Time").text = Global.format_time(self_timer.wait_time)
	get_node("../Opponent Time").text = Global.format_time(opponent_timer.wait_time)
	
	
	#Setup the pieces
	if(_pieces.size() > 0):
		pieces = _pieces
	
	_init_start_anim()

func _set_color(_col):
	player_color = _col
	
	if(player_color == Global.colors.YELLOW):
		opponent_color = Global.colors.RED
		get_node("Background").scale.y *= -1

func _construct_containers():
	get_node("Bottom Bear Container").construct(Global.pieces.BEAR, player_color)
	get_node("Bottom Deer Container").construct(Global.pieces.DEER, player_color)
	get_node("Bottom Doe Container").construct(Global.pieces.DOE, player_color)
	get_node("Bottom Wall Container").construct(Global.pieces.WALL, player_color)
	
	get_node("Top Bear Container").construct(Global.pieces.BEAR, opponent_color)
	get_node("Top Deer Container").construct(Global.pieces.DEER, opponent_color)
	get_node("Top Doe Container").construct(Global.pieces.DOE, opponent_color)
	get_node("Top Wall Container").construct(Global.pieces.WALL, opponent_color)
	
func _enable_containers(par):
	get_node("Bottom Bear Container").enable_pieces(par)
	get_node("Bottom Deer Container").enable_pieces(par)
	get_node("Bottom Doe Container").enable_pieces(par)
	get_node("Bottom Wall Container").enable_pieces(par)

func _init_start_anim():
	anim = load("res://Board/Place Your Pieces Animation.tscn").instance()
	anim.connect("animation_finished", self, "_destroy_anim")
	if(player_color == Global.colors.RED):
		add_child(anim)
		anim.play("SlideToCenter")

func _destroy_anim(_a):
	remove_child(anim)
	anim.queue_free()

func _process(_delta):
	if(current_state == states.PLAYER):
		get_node("../Player Time").text = Global.format_time(self_timer.time_left)
		get_node("../Opponent Time").text = Global.format_time(opponent_timer.wait_time)
	else:
		get_node("../Player Time").text = Global.format_time(self_timer.wait_time)
		get_node("../Opponent Time").text = Global.format_time(opponent_timer.time_left)


func _on_piece_dropped(piece):
	
	_hide_available_tiles()
	
	if(board_rect.has_point(piece.global_position)):
		var x = int((piece.global_position.x - board_rect.position.x)/ tile_size)
		var y = int((piece.global_position.y - board_rect.position.y)/ tile_size)
		if(y > 4 and board.get_tile_type(x, y) == Global.pieces.EMPTY):
			#put the piece in the appropriate tile
			board.set_tile(Vector2(x, y), piece.get_class(), player_color)
			piece.set_global_pos(Vector2(x*tile_size+tile_size/2,
			y*tile_size+tile_size/2) + board_rect.position)
			piece.tile_pos = Vector2(x, y)
			piece.enabled = false
			pieces.append(piece)
			
			#the move is opponent's now
			self_timer.stop()
			opponent_timer.start(60)
			_enable_containers(false)
			current_state = states.OPPONENT
			network.rpc_id(1, "placed_a_piece", piece.get_class(), Vector2(x, y))
			return
	
	#board is occupied or the piece was dropped outside the board
	#return the piece to its container
	var container = get_node("Bottom " +Global.piece_str(piece.get_class())+ " Container")
	piece.set_global_pos(container.global_position + Vector2(tile_size/2, tile_size/2))
	piece.tile_pos = Vector2(-1, -1)
	container.piece_got_dropped(piece)

func _on_piece_grabbed(piece):
	if(piece.tile_pos.x == -1 and piece.tile_pos.y == -1):
		#inform the appropriate container
		var container = get_node("Bottom " + Global.piece_str(piece.get_class()) + " Container")
		container.piece_got_grabbed()
		_show_available_tiles()
		return
	board.set_tile(piece.tile_pos, Global.pieces.EMPTY, player_color)
	_show_available_tiles()

func _on_piece_clicked(piece):
	pass#TODO


func _show_available_tiles():
	var inst
	for j in range(5, 8):
		for i in range(0, 8):
			if(board.get_tile_type(i, j) == Global.pieces.EMPTY):
				inst = green_sprite_scene.instance()
				inst.position = Vector2(tile_size*i+tile_size/2, tile_size*j+tile_size/2) + board_rect.position
				current_sprites.append(inst)
				add_child(inst)
			else:
				inst = red_sprite_scene.instance()
				inst.position = Vector2(tile_size*i+tile_size/2, tile_size*j+tile_size/2) + board_rect.position
				inst.z_index = 1
				current_sprites.append(inst)
				add_child(inst)
	for j in range(0, 5):
		for i in range(0, 8):
			inst = red_sprite_scene.instance()
			inst.position = Vector2(tile_size*i+tile_size/2, tile_size*j+tile_size/2) + board_rect.position
			inst.z_index = 1
			current_sprites.append(inst)
			add_child(inst)

func _hide_available_tiles():
	for sprite in current_sprites:
		sprite.queue_free()
	current_sprites.clear()


func disconnect_piece_signals():
	for piece in pieces:
		if(piece.color == player_color):
			piece.disconnect("grabbed", self, "_on_piece_grabbed")
			piece.disconnect("dropped", self, "_on_piece_dropped")
			piece.disconnect("clicked", self, "_on_piece_clicked")


#the parameters are the piece and the coordinate of the opponent's move
func opponent_placed_a_piece(piece_type, coordinate):
	var piece_type_str = Global.piece_str(piece_type)
	
	get_node("Top "+piece_type_str+" Container").decrement_piece_count()
	
	var piece = load("res://Board/Pieces/"+piece_type_str+".tscn").instance()
	get_node("Top "+piece_type_str+" Container").add_child(piece)
	
	var tile_pos = Vector2(7 - coordinate.x, 7 - coordinate.y)
	var global_pos = Vector2(board_rect.end.x - coordinate.x*tile_size - tile_size/2,
		 board_rect.end.y - coordinate.y*tile_size - tile_size/2)
	piece.construct(opponent_color, tile_pos, global_pos, false)
	
	pieces.append(piece)
	
	board.set_tile(tile_pos, piece_type, opponent_color)


func players_turn():
	current_state = states.PLAYER
	opponent_timer.stop()
	self_timer.start(60)
	_enable_containers(true)


