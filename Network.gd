extends Node

var start_menu : Control
var board : Node2D

var popup : PopupDialog

#Board Info
var my_info = {name = "Shocking!", color = Global.colors.RED}
var opponent_name : String
var player_time:float = 60
var opponent_time:float = 60
var current_phase:int = Global.phases.PREGAME
var pieces = []


func _ready():
	start_menu = load("res://Start Menu/StartMenu.tscn").instance()
	add_child(start_menu)
	start_menu.connect("start_button_pressed", self, "connect_to_server")
	
#	get_tree().connect("network_peer_connected", self, "_player_connected")
#	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
#	get_tree().connect("connection_failed", self, "_connected_fail")
#	get_tree().connect("server_disconnected", self, "_server_disconnected")
	
	popup = load("res://Start Menu/WaitingPopup.tscn").instance()
	add_child(popup)



func connect_to_server():
	if(get_tree().network_peer == null):
		my_info["name"] = start_menu.get_username()
		var ip  = start_menu.get_server_ip()
		var port = 7000
		var split_ip = ip.split(":")
		if(split_ip.size() == 2):
			ip = split_ip[0]
			port = int(split_ip[1])
		
		var peer = NetworkedMultiplayerENet.new()
		peer.create_client(ip, port)
		get_tree().network_peer = peer
		
		popup.get_node("Label").text = "Waiting for the server"
		popup.popup_centered()


func _connected_ok():
	rpc_id(1, "register_player", my_info)
	popup.get_node("Label").text = "Waiting for \nthe second player"
	popup.popup_centered()


func _switch_to_board():
	popup.queue_free()
	start_menu.queue_free()
	board = load("res://Board/Board.tscn").instance()
	add_child(board)
	board.construct(my_info["name"], opponent_name, my_info["color"], player_time, opponent_time, current_phase, pieces)


remote func set_board_info(_opponent_name, _player_color, _player_time = null, _opponent_time = null,
		_current_phase = null, _tile_pos = null, _types = null, _colors = null):
	
	opponent_name = _opponent_name
	my_info["color"] = _player_color
	
	if(_player_time == null):#if only three parameters were given
		current_phase = Global.phases.PREP
		_switch_to_board()
		return
	
	current_phase = _current_phase
	
	pieces = []
	
	var deer = load("res://Deer.tscn")
	var doe = load("res://Doe.tscn")
	var bear = load("res://Bear.tscn")
	var wall = load("res://Wall.tscn")
	
	for i in range(_tile_pos.size()):
		
		var new_piece
		if(_types[i] == Global.pieces.DEER):
			new_piece = deer.instance()
		elif(_types[i] == Global.pieces.DOE):
			new_piece = doe.instance()
		elif(_types[i] == Global.pieces.BEAR):
			new_piece = bear.instance()
		elif(_types[i] == Global.pieces.WALL):
			new_piece = wall.instance()
		
		new_piece._set_color(_colors[i])
		new_piece.tile_pos = _tile_pos[i]
		
		pieces.append(new_piece)
	
	_switch_to_board()

remote func switch_to_battle_phase():
	current_phase = Global.phases.BATTLE
	board.switch_to_battle_phase()

### Prep Phase Remote Functions###
remote func opponent_placed_a_piece(piece_type, coordinate):
	board.opponent_placed_a_piece(piece_type, coordinate)

remote func players_turn():
	board.players_turn()


### Battle Phase Remote Functions ###
remote func opponent_moved(from_adjusted, to_adjusted):
	board.opponent_moved(from_adjusted, to_adjusted)

remote func set_time(first_timer, second_timer):
	board.set_time(first_timer, second_timer)

remote func game_ends(winner):
	board.game_ends(winner)






