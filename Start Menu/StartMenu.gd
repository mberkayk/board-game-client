extends Control

signal start_button_pressed

# Called when the node enters the scene tree for the first time.
func _ready():
	$"Input Panel/VBoxContainer/VBoxContainer/Name Container/Name".grab_focus()

func get_username():
	return get_node("Input Panel/VBoxContainer/VBoxContainer/Name Container/Name").text
	
	
func get_server_ip():
	return get_node("Input Panel/VBoxContainer/VBoxContainer/Server Container/Server Ip").text


func _on_Enter_Game_pressed():
	if(get_username() != ""):
		emit_signal("start_button_pressed")


func _on_Name_text_entered(_new_text):
	if(get_username() != ""):
		emit_signal("start_button_pressed")


func _on_Server_Ip_text_entered(_new_text):
	if(get_username() != ""):
		emit_signal("start_button_pressed")
