class_name Global

enum colors {RED = 8, YELLOW = 16}
enum phases {PREGAME, PREP, BATTLE}
enum pieces {EMPTY = 0, DOE = 1, DEER = 2, BEAR = 3, WALL = 4}

static func format_time(time):
	var minutes = str(int(time)/60)
	if(minutes.length() == 1):
		minutes = "0"+minutes
		
	var seconds = str(int(time)%60)
	if(seconds.length() == 1):
		seconds = "0"+seconds
		
	return minutes+":"+seconds

static func piece_str(piece_type):
	var enum_str = pieces.keys()[piece_type]
	return enum_str[0] + enum_str.substr(1).to_lower()
